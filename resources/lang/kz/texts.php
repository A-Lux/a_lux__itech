<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Страница не найдена'                                   => 'Бет табылмады',
    'Извините, страница, которую вы ищете, не найдена'      => 'Кешіріңіз, сіз іздеген бет табылмады.',
    'Вернуться на главную'                                  => 'Басты бетке оралу',
    'Главная'                                               => 'Басты бет',
    'Написать'                                              => 'Жіберу',
    'Ваше имя'                                              => 'Аты-жөніңіз',
    'Ваш e-mail'                                            => 'Е-mail поштаңыз',
    'Ваше сообщение'                                        => 'Сіздің хабарламаңыз',
    'Подробнее'                                             => 'Толығырақ',
    'Смотреть все'                                          => 'Барлығын көру',
    'Номер телефона'                                        => 'Телефон нөмір*   мысалы: +77053435566',
    'Даю согласие на обработку своих данных'                => 'Мен деректерімді өңдеуге келісемін',
    'Ознакомиться'                                          => 'Толығырақ',
    'Читать подробнее'                                      => 'Ары қарай оқу',
    'Заработная плата'                                      => 'Еңбекақы',
    'Требуемый опыт'                                        => 'Тәжірибе',
    'Обязанности'                                           => 'Міндеттер',
    'Продукция'                                             => 'Өнімдер',
    'Решения'                                               => 'Шешімдер',
    'Заявка успешно отправлен'                              => 'Өтініш сәтті жіберілді!',
    'В ближайшее время мы свяжемся с вами'                  => 'Жақын арада сізбен хабарласамыз.',
];
