@extends('layouts.app')
@section('content')

    <div class="banner solution-banner" style="background-image: url({{ Voyager::image($page->getPage()->banner) }});">
        <div class="container">
            <h1>{{ $page->getPage()->getFirstTitle() }}</h1>
        </div>
    </div>

    @include('/partials/breadcrumbs')

    <div class="solution">
        <div class="container">
            @foreach($solutions as $solution)
            <div class="solution-inner {{ $loop->last ? "solution-bottom" : "" }}">
                <div class="solution-image">
                    <img src="{{ Voyager::image($solution->image) }}" alt="">
                </div>
                <div class="solution-content">
                    <h1>{{ $solution->name }}</h1>
                    {!! $solution->short_description !!}
                    <a href="{{ route('solution', ['id' => $solution->id]) }}">@lang('texts.Читать подробнее')...</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
