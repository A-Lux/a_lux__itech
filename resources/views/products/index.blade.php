@extends('layouts.app')
@section('content')

    <div class="banner products-banner" style="background-image: url({{ Voyager::image($page->getPage()->banner) }});">
        <div class="container">
            <h1>{{ $page->getPage()->getFirstTitle() }}</h1>
        </div>
    </div>

    @include('/partials/breadcrumbs')

    <div class="products">
        <div class="container">
            <div class="row">
                @foreach($products as $product)
                <div class="col-xl-4 col-md-6">
                    <div class="products-info {{ strlen($product->title) > 70 ? "products-info-6" : "" }}">
                        <h1>{{ $product->title }} </h1>
                        <div class="products-content" style="background-image: url({{ Voyager::image($product->image) }});background-size: cover;">
                            <div class="products-text">
                                <p>{{ $product->name }}</p>
                                <a href="{{ route('product', ['id' => $product->id]) }}">@lang('texts.Ознакомиться')</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
