@extends('layouts.app')
@section('content')

    @include('/partials/breadcrumbs')

    <div class="letters-page">
        <div class="container">
            <h1 class="title-info">{{ $page->getPage()->getFirstTitle() }}</h1>
            <div class="letters-slider">
                @php $m = 0; @endphp
                @foreach($letters as $v)
                    @php $m++; @endphp
                    @if($m % 8 == 1)
                    <div class="letters">
                        <div class="row">
                    @endif
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="letters-image">
                                <a href="{{ Voyager::image($v->image) }}" class="letters-zoom">
                                    <img src="{{ Voyager::image($v->image) }}" alt="">
                                    <div class="zoom-in-letter">
                                        <img src="{{ asset('images/zoom-in-letters.png') }}" alt="">
                                        @if($v->description != null)
                                            <p>{{ $v->description }}</p>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        </div>
                    @if($m % 8 == 0)
                        </div>
                    </div>
                    @endif
                @endforeach

                @if($m % 8 != 0)
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection
