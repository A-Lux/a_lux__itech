@extends('layouts.app')
@section('content')

    @include('/partials/breadcrumbs')

    <div class="partners-page ">
        <div class="container">

            <h1 class="title-info">{{ $page->getPage()->getFirstTitle() }}</h1>
            {!! $content->content !!}

            <div class="partners">
                <div class="container">
                    @include('/partials/partners', compact('partners'))
                </div>
            </div>
        </div>
    </div>

@endsection
