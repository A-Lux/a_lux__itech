<div class="partners-slider">
    @foreach($partners as $v)
        <div class="partner">
            <img src="{{ Voyager::image($v->image) }}" alt="">
        </div>
    @endforeach
</div>
