<div class="{{ $class }}">
    <div class="container">
        <div class="header-links">
            <nav>
                @if(Request::segment(1))
                <a href="{{ route('home') }}" class="logo">
                    <img src="{{ $logo }}" alt="">
                </a>
                @else
                    <img src="{{ $logo }}" alt="">
                @endif

                @foreach (menu('site_header', '_json') as $menuItem)
                    @php $menuItem = $menuItem->translate(app()->getLocale()) @endphp
                    @if($_SERVER["REQUEST_URI"] == $menuItem->url)
                        <li><a href="#">{{ $menuItem->title }}</a></li>

                    @else
                        <li><a href="{{ $menuItem->url }}">{{ $menuItem->title }}</a></li>
                    @endif
                @endforeach

                <select onchange="location = this.options[this.selectedIndex].value;">
                    <option value="{{ route('lang', ['locale' => 'ru']) }}" {{ app()->isLocale('ru') ? "selected":"" }}>RU</option>
                    <option value="{{ route('lang', ['locale' => 'kz']) }}" {{ app()->isLocale('kz') ? "selected":"" }}>KZ</option>
                </select>
            </nav>
        </div>
    </div>
</div>
<div class="burger-menu-image">
    @if(Request::segment(1))
        <a href="{{ route('home') }}">
            <img src="{{ $logo }}" alt="">
        </a>
    @else
        <img src="{{ $logo }}" alt="">
    @endif
</div>
<div class="burger-menu-content">
    <a href="#" class="burger-menu-btn">
        <span class="burger-menu-lines"></span>
    </a>
</div>
<div class="nav-panel-mobil">
    <div class="container">
        <nav class="navbar-expand-lg navbar-light">
            <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">

                @foreach (menu('site_header', '_json') as $menuItem)
                    @php $menuItem = $menuItem->translate(app()->getLocale()) @endphp
                    @if($_SERVER["REQUEST_URI"] == $menuItem->url)
                        <li class="nav-item"><a class="nav-link" href="#">{{ $menuItem->title }}</a></li>

                    @else
                        <li class="nav-item"><a class="nav-link" href="{{ $menuItem->url }}">{{ $menuItem->title }}</a></li>
                    @endif
                @endforeach

                <select onchange="location = this.options[this.selectedIndex].value;">
                    <option value="{{ route('lang', ['locale' => 'ru']) }}" {{ app()->isLocale('ru') ? "selected":"" }}>RU</option>
                    <option value="{{ route('lang', ['locale' => 'kz']) }}" {{ app()->isLocale('kz') ? "selected":"" }}>KZ</option>
                </select>
            </ul>
        </nav>
    </div>
</div>
