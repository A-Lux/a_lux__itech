@extends('layouts.app')
@section('content')

@include('/partials/breadcrumbs')

<div class="contacts">
    <div class="container">
        <div class="contact-top">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="contacts-left">
                        <h1>{{ $page->getPage()->getFirstTitle() }}</h1>
                        <div class="contacts-inner">
                            <div class="contacts-sub">
                                <img src="{{ asset('images/contact-map.png') }}" alt="">
                                <p>{{ $address->content }}</p>
                            </div>
                            <div class="contacts-sub">
                                <img src="{{ asset('images/contact-phone.png') }}" alt="">
                                @foreach($telephones as $v)
                                    <a href="tel:{{ $v->text }}">{{ $v->text }}  @if(!$loop->last)/ @endif</a>
                                @endforeach
                            </div>
                            <div class="contacts-sub contact-email">
                                <img src="{{ asset('images/contact-email.png') }}" alt="">
                                <a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 p-0">
                    <div class="contacts-right">
                        @if(app()->isLocale('kz'))
                            @php $lang = 'kz_KZ' @endphp
                        @else
                            @php $lang = 'ru_RU' @endphp
                        @endif
                        <iframe class="ggmap_contact" src="{{ setting('site.map_url') }}?lang={{ $lang }}" frameborder="0"
                            style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts-bottom">
            <h1>{!! $page->getPage()->getSecondTitle() !!}</h1>

            <form class="row">
                <div class="col-xl-5 col-md-5">
                    <div class="contacts-left">
                        <input type="text" name="name" placeholder="@lang('texts.Ваше имя')* ">
                        <input type="text" name="email" placeholder="@lang('texts.Ваш e-mail')* ">
                        <button id="contactFeedbackBtn" type="button">@lang('texts.Написать')</button>
                    </div>
                </div>
                <div class="col-xl-7 col-md-7">
                    <div class="contacts-right">
                        <textarea name="message" cols="30" rows="10"
                            placeholder="@lang('texts.Ваше сообщение')"></textarea>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

@endsection
