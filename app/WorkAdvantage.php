<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class WorkAdvantage extends Model
{
    use Translatable;
    protected $translatable = ['name', 'content'];

    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }
}
