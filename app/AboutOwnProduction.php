<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutOwnProduction extends Model
{
    use Translatable;
    protected $translatable = ['content'];

    public static function getContent(){
        return self::first();
    }
}
