<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Product extends Model
{
    use Translatable;
    protected $translatable = ['title', 'name', 'meta_title', 'meta_description', 'meta_keyword'];

    public static function getAll(){
        return self::where('status', 1)->orderBy('sort', 'ASC')->get();
    }
}
