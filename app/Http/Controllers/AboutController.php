<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.2020
 * Time: 14:13
 */

namespace App\Http\Controllers;

use App\AboutOwnProduction;
use App\Helpers\TranslatesCollection;
use App\KeyArea;
use App\Partner;

class AboutController extends Controller
{
    public function index(){

        $production = AboutOwnProduction::getContent();
        $partners = Partner::getAll();
        $key_areas = KeyArea::getAll();

        TranslatesCollection::translate($production, app()->getLocale());
        TranslatesCollection::translate($key_areas, app()->getLocale());

        return view('about.index', compact('production', 'partners', 'key_areas'));

    }
}
