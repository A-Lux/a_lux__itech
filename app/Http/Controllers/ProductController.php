<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.06.2020
 * Time: 11:43
 */

namespace App\Http\Controllers;

use App\Helpers\TranslatesCollection;
use App\Product;
use App\ProductContent;

class ProductController extends Controller
{
    public function index(){

        $products = Product::getAll();
        TranslatesCollection::translate($products, app()->getLocale());

        return view('products.index', compact('products'));
    }

    public function view($id){

        $product = Product::find($id);
        if($product && $product->status){

            $blocks = ProductContent::getAllByProduct($id);
            TranslatesCollection::translate($product, app()->getLocale());
            TranslatesCollection::translate($blocks, app()->getLocale());

            return view('products.view', compact('product', 'blocks'));
        }else{
            abort(404);
        }
    }
}
