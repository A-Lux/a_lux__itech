<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.06.2020
 * Time: 12:06
 */

namespace App\Http\Controllers;

use App\Helpers\TranslatesCollection;
use App\LettersOfRecommendation;

class LetterController extends Controller
{
    public function index(){

        $letters = LettersOfRecommendation::getAll();
        TranslatesCollection::translate($letters, app()->getLocale());

        return view('letters.index', compact('letters'));
    }
}
