<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 28.01.2020
 * Time: 16:27
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class MainAboutController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }
}
