<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 28.01.2020
 * Time: 17:11
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class MainCertificatesLicenseController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
