<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.06.2020
 * Time: 12:09
 */

namespace App\Http\Controllers;


use App\Helpers\TranslatesCollection;
use App\Vacancy;

class VacancyController extends Controller
{

    public function index(){

        $vacancies = Vacancy::getAll();
        TranslatesCollection::translate($vacancies, app()->getLocale());

        return view('vacancies.index', compact('vacancies'));
    }
}
