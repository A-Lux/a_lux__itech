<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class MainCertificatesLicense extends Model
{
    use Translatable;
    protected $translatable = ['left_title', 'left_content', 'right_title', 'right_content'];

    public static function getContent(){
        return self::first();
    }
}
