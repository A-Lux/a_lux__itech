<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AboutSlider extends Model
{
    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }
}
