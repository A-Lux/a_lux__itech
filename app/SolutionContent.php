<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class SolutionContent extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content', 'check_list_content', 'dot_list_left', 'dot_list_right'];

    public function relations(){
        return $this->hasMany('App\SolutionContentImageList', 'solution_content_id', 'id');
    }

    public static function getAllByProduct($id){
        return self::where('solution_id', $id)->with('relations')->orderBy('sort', 'ASC')->get();
    }
}
