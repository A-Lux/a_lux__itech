<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Solution extends Model
{
    use Translatable;
    protected $translatable = ['scale', 'name', 'meta_title', 'meta_description', 'meta_keyword', 'technical_difficulty', 'short_description'];

    public static function getAll(){
        return self::where('status', 1)->orderBy('sort', 'ASC')->get();
    }
}
