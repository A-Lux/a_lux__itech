<?php

namespace App\Providers;



use App\Address;
use App\Copyright;
use App\Footer;
use App\Helpers\TranslatesCollection;
use App\Page\Page;
use App\Product;
use App\Project;
use App\Service;
use App\Solution;
use App\Telephone;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Page $page, Product $product, Solution $solution, Address $address,
                         Copyright $copyright, Telephone $telephone)
    {

        View::composer('*', function($view) use($page) {
            $view->with(['page' => $page]);
        });

        View::composer('*', function($view) use($product) {
            $product = $product::find( Request::segment(2));
            if($product){
                TranslatesCollection::translate($product, app()->getLocale());
                $view->with(['g_product' => $product]);
            }

        });

        View::composer('*', function($view) use($solution) {
            $solution = $solution::find( Request::segment(2));
            if($solution){
                TranslatesCollection::translate($solution, app()->getLocale());
                $view->with(['g_solution' => $solution]);
            }

        });

        View::composer('*', function($view) use($address) {
            $address = $address::getContent();
            TranslatesCollection::translate($address, app()->getLocale());
            $view->with(['address' => $address]);
        });


        View::composer('*', function($view) use($copyright) {
            $copyright = $copyright::getContent();
            TranslatesCollection::translate($copyright, app()->getLocale());
            $view->with(['copyright' => $copyright]);
        });

        View::composer('*', function($view) use($telephone) {
            $telephones = $telephone::getAll();
            $view->with(['telephones' => $telephones]);
        });


    }
}
