
//products
if($('[name=status]').val() == 1){
    $('[name=content]').closest('div').show();

    $('[data-get-items-field="product_content_belongstomany_product_image_list_relationship"]').closest('div').hide();
    $('[data-get-items-field="solution_content_belongstomany_solution_image_list_relationship"]').closest('div').hide();
    $('[name=picture_list_image]').closest('div').hide();
    $('[name=check_list_content]').closest('div').hide();
    $('[name=check_list_image]').closest('div').hide();
    $('[name=dot_list_left]').closest('div').hide();
    $('[name=dot_list_right]').closest('div').hide();
}else if($('[name=status]').val() == 2){
    $('[data-get-items-field="product_content_belongstomany_product_image_list_relationship"]').closest('div').show();
    $('[data-get-items-field="solution_content_belongstomany_solution_image_list_relationship"]').closest('div').show();
    $('[name=picture_list_image]').closest('div').show();

    $('[name=content]').closest('div').hide();
    $('[name=check_list_content]').closest('div').hide();
    $('[name=check_list_image]').closest('div').hide();
    $('[name=dot_list_left]').closest('div').hide();
    $('[name=dot_list_right]').closest('div').hide();
}else if($('[name=status]').val() == 3){
    $('[name=check_list_content]').closest('div').show();
    $('[name=check_list_image]').closest('div').show();

    $('[name=content]').closest('div').hide();
    $('[data-get-items-field="product_content_belongstomany_product_image_list_relationship"]').closest('div').hide();
    $('[data-get-items-field="solution_content_belongstomany_solution_image_list_relationship"]').closest('div').hide();
    $('[name=picture_list_image]').closest('div').hide();
    $('[name=dot_list_left]').closest('div').hide();
    $('[name=dot_list_right]').closest('div').hide();
}else if($('[name=status]').val() == 4){
    $('[name=dot_list_left]').closest('div').show();
    $('[name=dot_list_right]').closest('div').show();

    $('[name=content]').closest('div').hide();
    $('[data-get-items-field="product_content_belongstomany_product_image_list_relationship"]').closest('div').hide();
    $('[data-get-items-field="solution_content_belongstomany_solution_image_list_relationship"]').closest('div').hide();
    $('[name=picture_list_image]').closest('div').hide();
    $('[name=check_list_content]').closest('div').hide();
    $('[name=check_list_image]').closest('div').hide();
}


$('body').on('change', '[name=status]', function () {
    if($(this).val() == 1){
        $('[name=content]').closest('div').show();

        $('[data-get-items-field="product_content_belongstomany_product_image_list_relationship"]').closest('div').hide();
        $('[data-get-items-field="solution_content_belongstomany_solution_image_list_relationship"]').closest('div').hide();
        $('[name=picture_list_image]').closest('div').hide();
        $('[name=check_list_content]').closest('div').hide();
        $('[name=check_list_image]').closest('div').hide();
        $('[name=dot_list_left]').closest('div').hide();
        $('[name=dot_list_right]').closest('div').hide();
    }else if($(this).val() == 2){
        $('[data-get-items-field="product_content_belongstomany_product_image_list_relationship"]').closest('div').show();
        $('[data-get-items-field="solution_content_belongstomany_solution_image_list_relationship"]').closest('div').show();
        $('[name=picture_list_image]').closest('div').show();

        $('[name=content]').closest('div').hide();
        $('[name=check_list_content]').closest('div').hide();
        $('[name=check_list_image]').closest('div').hide();
        $('[name=dot_list_left]').closest('div').hide();
        $('[name=dot_list_right]').closest('div').hide();
    }else if($(this).val() == 3){
        $('[name=check_list_content]').closest('div').show();
        $('[name=check_list_image]').closest('div').show();

        $('[name=content]').closest('div').hide();
        $('[data-get-items-field="product_content_belongstomany_product_image_list_relationship"]').closest('div').hide();
        $('[data-get-items-field="solution_content_belongstomany_solution_image_list_relationship"]').closest('div').hide();
        $('[name=picture_list_image]').closest('div').hide();
        $('[name=dot_list_left]').closest('div').hide();
        $('[name=dot_list_right]').closest('div').hide();
    }else if($(this).val() == 4){
        $('[name=dot_list_left]').closest('div').show();
        $('[name=dot_list_right]').closest('div').show();

        $('[name=content]').closest('div').hide();
        $('[data-get-items-field="product_content_belongstomany_product_image_list_relationship"]').closest('div').hide();
        $('[data-get-items-field="solution_content_belongstomany_solution_image_list_relationship"]').closest('div').hide();
        $('[name=picture_list_image]').closest('div').hide();
        $('[name=check_list_content]').closest('div').hide();
        $('[name=check_list_image]').closest('div').hide();
    }
});

