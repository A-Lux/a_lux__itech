$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});




$('body').on('click','#mainFeedbackBtn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/main',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $(".contacts-right")[0].reset();
                Swal.fire(response.title, response.content, 'success');
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});



$('body').on('click','#contactFeedbackBtn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/request/contact',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                $(".contacts-bottom form")[0].reset();
                Swal.fire(response.title, response.content, 'success');
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});


